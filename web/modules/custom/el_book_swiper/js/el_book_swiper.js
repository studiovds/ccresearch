(function ($, Drupal) {
  const swiper = new Swiper('.swiper-container', {
    direction: 'horizontal',
    autoHeight: true,
    loop: false,
    longSwipes: false,
    history: {
      key: '',
      replaceState: false,
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  swiper.on('slidePrevTransitionEnd', function () {
    getPrevBookNodes(swiper);
  });
  swiper.on('slideNextTransitionEnd', function () {
    getNextBookNodes(swiper);
  });

  function getNextBookNodes(swiper) {
    var el_book_swiper = $("#el-book-swiper");
    if (swiper.slides.length < parseInt(el_book_swiper.data('total'))) {
      // below url is set in routing.yml
      $.get("/el_book_swiper/nodes/" + $('.swiper-slide').last().data('node') + "/1", function (data) {
        swiper.appendSlide(data);
      });
    }
  }

  function getPrevBookNodes(swiper) {
    var el_book_swiper = $("#el-book-swiper");
    if (swiper.slides.length < parseInt(el_book_swiper.data('total'))) {
      // below url is set in routing.yml
      $.get("/el_book_swiper/nodes/" + $('.swiper-slide').first().data('node') + "/0", function (data) {
        swiper.prependSlide(data);
      });
    }
  }
})(jQuery, Drupal);


jQuery(document).ready(function (){
  jQuery('.el-swiper-nav-links a').on('click', function (e){
    e.preventDefault();
    var el_swipe_link = jQuery(this);
    if (el_swipe_link.length > 0) {
      // below url is set in routing.yml
      jQuery.get(el_swipe_link.attr('href'), function (data) {
        var newDoc = document.open("text/html", "replace");
        newDoc.write(data);
        newDoc.close();
      });
    }
    return false;
  });
});
