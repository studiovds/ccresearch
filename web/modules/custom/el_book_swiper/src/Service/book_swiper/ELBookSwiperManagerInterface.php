<?php

  namespace Drupal\el_book_swiper\Service\book_swiper;


  /**
   * Interface WebformBookingManagerInterface.
   */
  interface ELBookSwiperManagerInterface {

    /**
     * @param null $nid
     *
     * @return \Drupal\Core\Entity\EntityInterface|null
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function getBookData($nid = NULL, $type = 'issue', $preload = NULL);


    /**
     * @param $items
     * @param $node_id
     *
     * @return mixed
     */
    public function preloadItemsAtInit($items, $node_id);

    /**
     * @param null $node_id
     *
     * @return mixed
     */
    public function getSwiperSlideHtml($node_id = NULL);

    /**
     * @param array $target_ids
     *
     * @return array
     */
    public static function fetchIDs($target_ids);

  }
