<?php

  namespace Drupal\el_book_swiper\Service\book_swiper;

  use Drupal\book\BookManagerInterface;
  use Drupal\Core\Config\ConfigFactoryInterface;
  use Drupal\Core\Entity\EntityTypeManagerInterface;

  /**
   * Class ELBookSwiperManager.
   */
  class ELBookSwiperManager implements ELBookSwiperManagerInterface {

    /**
     * The config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * Drupal\book\BookManager.
     *
     * @var \Drupal\book\BookManagerInterface
     */
    protected $bookManager;

    /**
     * Drupal\Core\Entity\EntityTypeManagerInterface definition.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Constructs a new WebformBookingManager object.
     *
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   EntityTypeManager.
     */
    public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, BookManagerInterface $bookManager) {
      $this->configFactory = $config_factory;
      $this->entityTypeManager = $entity_type_manager;
      $this->bookManager = $bookManager;
    }

    /**
     * Create Book tree
     * @param null $nid
     *
     * @return \Drupal\Core\Entity\EntityInterface|null
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function getBookData($nid = NULL, $type = 'issue', $preload = NULL) {
      $results = NULL;
      if (!is_null($nid) && is_numeric($nid)) {
        //if nid is not null and numeric
        if ($book_data = $this->bookManager->loadBookLink($nid)) {
          //if book link is available
          if (is_array($book_data) && isset($book_data['pid']) && isset($book_data['bid'])) {
            //if book data is available
            switch ($type) {
              case 'issue':
                //if type is issue
                $node =  $this->entityTypeManager->getStorage('node')->load($book_data['pid']);
                if (is_object($node) && $node->id()) {
                  $results = $node;
                }
                break;
              case 'reader':
                //if type is reader
                $node =  $this->entityTypeManager->getStorage('node')->load($book_data['bid']);
                if (is_object($node) && $node->id()) {
                  $results = $node;
                }
                break;
              case 'articles':
                //if type is article
                $results = [];
                $books = $this->bookManager->bookTreeAllData($book_data['bid']);
                $book = reset($books);
                if (is_array($book) && !empty($book)) {
                  foreach ($book['below'] as $issue) {
                    $issue_id = $issue['link']['nid'];
                    if (isset($issue['below']) && !empty($issue['below'])) {
                      $results = [
                        'title' => $issue['link']['title'],
                        'pages' => [],
                        'total' => count($issue['below']) + 1,
                        'batches' => [],
                        'nids' => [],
                        'nid' => (int)$issue_id,
                        'url' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/'. $issue_id),
                      ];
                      $index = 1;
                      foreach ($issue['below'] as $issue_page) {
                        if ((is_null($preload)) || (is_numeric($preload) && $index < $preload)) {
                          $issue_page_id = $issue_page['link']['nid'];
                          $results['nids'][] = $issue_page_id;
                          $results['pages'][$issue_page_id] = [
                            'title' => $issue_page['link']['title'],
                            'nid' => (int)$issue_page_id,
                            'url' => \Drupal::service('path_alias.manager')
                              ->getAliasByPath('/node/' . $issue_page_id),
                          ];
                          $index++;
                        }
                      }
                    }
                  }
                }
                break;
            }
          }
        }
      }

      return $results;
    }

    /**
     * Preload Surrounding Items on Swiper Init
     * @param $items
     * @param $node_id
     *
     * @return mixed
     */
    public function preloadItemsAtInit($items, $node_id) {
      //get key in array of current node
      $nid_index = array_search($node_id, $items['nids']);
      //get first key of array
      $first_key = array_key_first($items['nids']);
      //get last key of array
      $last_key = array_key_last($items['nids']);
      //add all items to $new_items container
      $new_items = $items;
      if ($nid_index >= $first_key && $nid_index <= $last_key) {
        //if current node is between first and last key in array
        $new_items['pages'] = [];
        //emtpy $new_items['pages'] container
        $new_items['nids'] = [];
        //emtpy $new_items['nids'] container
        $start = $nid_index - 2;
        //get key 2 items down
        $end = $nid_index + 2;
        //get key 2 items up
        if ($start < $first_key) $start = $first_key; //if 2 items down is smaller than current item reset to current item (happens at begin of swiper)
        if ($end > $last_key) $end = $last_key; //if 2 items up is bigger than current item reset to current item (happens at end of swiper)

        for ($i = $start; $i <= $end; $i++) {
          //loop trough items between start en end to get surrounding items of current node
          $current_nid = $items['nids'][$i];
          //add information to $new_items container
          $new_items['pages'][$current_nid] = $items['pages'][$current_nid];
          $new_items['nids'][] = $current_nid;
        }
      }

      return $new_items;
    }

    /**
     * Create Swiper HTML to output in Swiper
     * @param null $node_id
     *
     * @return mixed
     */
    public function getSwiperSlideHtml($node_id = NULL) {
      $build['node_template'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="swiper-slide" id="node-{{ slide.nid }}" data-node="{{ slide.nid }}" data-history="{{ slide.url }}">{{ drupal_entity(\'node\', slide.nid, \'swiperslide\') }}</div>',
        '#context' => [
          'slide' => $node_id,
        ],
      ];
      return \Drupal::service('renderer')->renderPlain($build);
    }


    /**
     * @param array $target_ids
     *
     * @return array
     */
    public static function fetchIDs($target_ids) {
      return array_map(function ($entry) {
        return $entry['target_id'];
      }, $target_ids);
    }

  }
