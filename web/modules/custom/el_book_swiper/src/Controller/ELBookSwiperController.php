<?php

namespace Drupal\el_book_swiper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ELBookSwiperController.
 */
class ELBookSwiperController extends ControllerBase
{

  /**
   * @return array
   */
  public function testingArea()
  {

    /** @var $el_book_swiper \Drupal\el_book_swiper\Service\book_swiper\ELBookSwiperManager */
    $el_book_swiper = \Drupal::service('el_book_swiper');
    $tree = $el_book_swiper->getBookData(11, 'articles', 5);

    $build['node_template'] = [
      '#type' => 'markup',
      '#markup' => 'Testing area123.',
    ];
    $build['el_book_swiper_template'] = [
      '#theme' => 'el_book_swiper_template',
      '#total' => $tree['total'],
      '#slide_items' => $tree['pages'],
    ];
    $build['#attached']['library'][] = 'el_book_swiper/el_book_swiper';
    return $build;
  }


  public function startBookSwipe($nid)
  {
    /** @var $el_book_swiper \Drupal\el_book_swiper\Service\book_swiper\ELBookSwiperManager */
    $el_book_swiper = \Drupal::service('el_book_swiper');
    //get book tree
    $tree = $el_book_swiper->getBookData($nid, 'articles');
    //get items to show on first swiper init
    $items = $el_book_swiper->preloadItemsAtInit($tree, $nid);

    $build['el_book_swiper_template'] = [
      '#theme' => 'el_book_swiper_template',
      '#total' => $items['total'],
      '#slide_items' => $items['pages'],
    ];

    $build['#attached']['library'][] = 'el_book_swiper/el_book_swiper';
    return $build;
  }


  public function getBookNodes($nid, $next = TRUE)
  {

    /** @var $el_book_swiper \Drupal\el_book_swiper\Service\book_swiper\ELBookSwiperManager */
    $el_book_swiper = \Drupal::service('el_book_swiper');
    //get book tree
    $tree = $el_book_swiper->getBookData($nid, 'articles');
    //get first key of array
    $first_key = array_key_first($tree['nids']);
    //get last key of array
    $last_key = array_key_last($tree['nids']);

    $return_array = [];

    if (isset($tree['nids']) && !empty($tree['nids'])) {
      //if tree exists and is not empty
      $key = array_search($nid, $tree['nids']);

      if ($next) {
        //if swiper is going to next slide
        $start = $key + 1; //add slide from 1 item after last slide at end of swiper
        $end = $key + 1; //stop adding slide from 1 item after last slide at end of swiper
      } else {
        //if swiper is going to previous slide
        $start = $key - 1; //add slide from 1 item before first slide at end of swiper
        $end = $key - 1; //stop adding slide from 1 item before first slide at end of swiper
      }

      if ($start < $first_key) $start = $first_key; //if 2 items down is smaller than current item reset to current item (happens at begin of swiper)
      if ($end > $last_key) $end = $last_key; //if 2 items up is bigger than current item reset to current item (happens at end of swiper)

      for ($i = $start; $i <= $end; $i++) {
        if (isset($tree['nids'][$i])) {
          $node_id = $tree['nids'][$i];
          $slide = $tree['pages'][$node_id];
          $return_array[] = $el_book_swiper->getSwiperSlideHtml($slide);
        }
      }
    }

    return new JsonResponse($return_array);
  }
}
