<?php

namespace Drupal\el_book_swiper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'IssuePageLinks' block.
 *
 * @Block(
 *  id = "issue_page_links",
 *  admin_label = @Translation("Issue page Links block"),
 * )
 */
class IssuePageLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#cache' => [
        'max-age' => 0,
      ]
    ];
    // Drupal has fetched the entity for us already.
    /** @var \Drupal\node\Entity\Node $node */
    $node = \Drupal::routeMatch()->getParameter('node');

    if (is_object($node) && $node->id() && $node->getType() == 'issue') {

      $items = [];
      /** @var $el_book_swiper \Drupal\el_book_swiper\Service\book_swiper\ELBookSwiperManager */
      $el_book_swiper = \Drupal::service('el_book_swiper');
      $tree = $el_book_swiper->getBookData($node->id(), 'articles');

      if (isset($tree['pages']) && !empty($tree['pages'])) {
        foreach ($tree['pages'] as $page) {
          $items[] = [
            '#markup' => Link::fromTextAndUrl($page['title'], Url::fromUri('internal:/node/' . $page['nid'], ['attributes' => [ 'class' => 'swiper-node-'.$page['nid']]]))->toString(),
            '#wrapper_attributes' => [
              'class' => [
                'el-swiper-nav-links'
              ],
            ],
          ];
        }
      }

      if (!empty($items)) {
        $build['items_list'] = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ];
      }
    }

    $build['#attached']['library'][] = 'el_book_swiper/el_book_swiper';
    return $build;
  }

}
