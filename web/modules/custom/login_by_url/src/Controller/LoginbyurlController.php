<?php

namespace Drupal\login_by_url\Controller;

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Utility\Html;

/**
 * Controller routines for login_by_url routes.
 *
 */
class LoginbyurlController extends ControllerBase {

  /**
   * This is the function that actually performs the login.
   *
   * @param string $urlstring
   *   login string from URL.
   * @param string $arg
   *
   *   The function first validates the URL login string.
   *   If good, then the user is logged in and transferred to the destination
   *   page. Otherwise they are taken to the front page. Results, good or bad,
   *   are logged with watchdog. If the intended user is already logged in,
   *   then redirect will occur even if link is outdated.
   */
  public function login($urlstring = 'none', $arg = NULL) {
    module_load_include('inc', 'login_by_url', 'login_by_url_security');
    // Sanitize.
    $urlstr = Html::escape($urlstring);
    $resultmsg = "";
    $user = User::load(\Drupal::currentUser()->id());
    $config = $this->config('login_by_url.settings');
    $codekey = $config->get('codekey');
    $codemin = $config->get('codemin');
    $uid = login_by_url_decode($urlstr, $codekey, $codemin, login_by_url_passphrase(),
      $resultmsg, $user->get('uid')->value
    );
    if ($uid > -1) {
      $account = login_by_url_testuid($uid, $resultmsg);
    }
    else {
      $account = NULL;
    }
    \Drupal::logger('login_by_url')->debug($resultmsg);
    if ($account != NULL) {
      // Find where to go: get rid of first two arguments and use the rest of
      // the URL as the destination.
      $current_path = \Drupal::service('path.current')->getPath();
      $args = explode('/', $current_path);
      unset($args[0]);
      unset($args[1]);
      unset($args[2]);
      $goto = implode('/', $args);

      // Maintain the original query string.
      $query = $_GET;
      unset($query['q']);
      if (count($query) > 0) {
        $goto .= '?' . implode('&', $query);
      }

      // Check in case this user is already logged in.
      $logged_in = ($user->get('uid')->value == $uid);
      if ($logged_in) {
        $resultmsg = t('User %username (%uid) was already logged in. Redirected to: %goto', ['%username' => $account->name, '%uid' => $uid, '%goto' => $goto]);
        \Drupal::logger('login_by_url')->notice($resultmsg);
      }
      // Log the user in.
      else {
        $account = User::load($uid);
        // Log in user. This function called by user_login_submit() which does
        // stuff that is not needed.
        user_login_finalize($account);
        $user = User::load(\Drupal::currentUser()->id());
        $logged_in = ($user->get('uid')->value == $uid);
        if ($logged_in) {
//          $resultmsg = t('Logging in as %username (%uid). Redirected to: %goto',
//            ['%username' => $account->name, '%uid' => $uid, '%goto' => $goto]);
          \Drupal::logger('login_by_url')->notice($resultmsg);
          // If persistent_login is installed, then set "remember me".
          if (\Drupal::moduleHandler()->moduleExists('persistent_login')) {
            _persistent_login_create_cookie($account);
          }
        }
        else {
          $resultmsg = t('Failed login as %username (%uid)',
            ['%username' => $account->name, '%uid' => $uid]);
        }
      }
      if ($logged_in) {
        $url = '/';
        $url .= implode('/', $args);
        $redirect = new RedirectResponse(Url::fromUserInput($url)->toString());
        $redirect->send();
      }
    }
    // Logs a notice.
    \Drupal::logger('login_by_url')->notice($resultmsg);
    if ($uid == -2) {
      $response = [
        '#markup' => '<h1>' . t('The link you used to access this page has expired.') . '</h1>' .
        '<p>' . t('If you have created a password, you can log on') . ' ' . Link::fromTextAndUrl(t('here'), Url::fromRoute('user.login'))
          ->toString() . '.</p>',
      ];
      return $response;
    }
    else {
      return $this->redirect('<front>');
    }
  }

}
