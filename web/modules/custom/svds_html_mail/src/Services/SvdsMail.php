<?php

namespace Drupal\svds_html_mail\Services;


/**
 * Class SvdsMail.
 */
class SvdsMail{

  /**
   * @var $route
   */
  protected $mailer;

  /**
   * @var $current_user
   */
  protected $current_user;

  /**
   * Constructor.
   *
   * @param $mailer
   * @param $current_user
   * @param $members
   */
  public function __construct($mailer, $current_user) {
    $this->mailer = $mailer;
    $this->current_user = $current_user;
  }

  /**
   * Sends the mails.
   * @param array $params
   */
  public function sendMail($params = array()) {

    // Build mail vars.
    $module = 'svds_html_mail';
    $key = 'svds_html_mail';
    $lang_code = $this->current_user->getPreferredLangcode();
    $params['base_url'] = \Drupal::request()->getSchemeAndHttpHost();
    $params['sender_name'] = $this->current_user->getAccountName();

    // Send emails.
    $result = $this->mailer->mail($module, $key, $params['mail_recipient'], $lang_code, $params, NULL, TRUE);

    if($result['result'] === true){
      \Drupal::logger('svds')->notice('Mail verstuurd naar: '.$params['name_recipient'].' over: '.$params['subject'].'.');
    } else {
      \Drupal::logger('svds')->notice('Mail NIET verstuurd naar: '.$params['name_recipient'].' over: '.$params['subject'].'.');
    }

  }

}
