# CC READER RESEARCH

## Inleiding
CC Reader is een platform om online publicaties te maken. Zie hiervoor https://cc-reader.nl. Deze wil ik opnieuw opbouwen in Drupal 9.

#### Het werkt nu (D7 versie)
Een gebruiker maakt een reader aan, zie dit als de magazine titel dus als voorbeeld de Quest. Hier kan de gebruiker een naam kiezen, een stijl, een subdomein of domeinnaam invoeren etc. De reader op zichzelf is nog niks, niet iets wat je kan bekijken.

Vervolgens maakt de gebruiker een editie aan deze wordt door een referentie gekoppeld aan het magazine. Dit is een editie van een magazine, dus als we vasthouden aan de Quest als voorbeeld dan is dit het november nummer. Hier kan een editie titel worden opgegeven en een cover beeld etc. Ook dit is op zichzelf nog een lege huls.

Als laatste kan de gebruiker artikelen aanmaken. Deze artikelen worden in een editie geplaatst doormiddel van een referentie. Uiteindelijk zorgen de artikelen er voor dat de editie gevuld wordt.

Een voorbeeld van zo’n magazine is: https://iederepatientisanders.verslagvandedag.nl/

## De nieuwe versie
Ik ben bezig om in deze Drupal omgeving de meeste belangrijke functionaliteiten in een module te vangen. Het is dus echt een test en onderzoek omgeving. Mijn huidige onderzoek is of ik de core Books module kan gebruiken voor een soort gelijke opzet als hierboven.

Ik heb dus net als bij de oude versie 3 content types. Reader, Editie en Artikel.
De reader is het hoofdniveau van het boek. Hier kan je op dit moment al gebruikers koppelen, dit zijn redacteuren die mee kunnen helpen aan het edited van dit book en zijn child nodes. En een domeinnaam opgeven. Wanneer je naar deze domeinnaam gaat krijg je de inhoud te zien van dit boek.

# CUSTOM MODULES
Ik heb verschillende modules om alle functionaliteiten werkend te krijgen:

## Book Access
Zie module README.md bestand

## Book Frontpage
Zie module README.md bestand

## Book Redirect
Zie module README.md bestand

## Theme Selection
Zie module README.md bestand

## Private Content
Zie module README.md bestand

## Login by URL
`login_by_url`: Maakt het mogelijk om direct op de reader in te loggen via een url. Elke gebruiker heeft zijn eigen URL.

#### Doen voor gebruik:
De role *Leesgroep* moet de rechten toegekend krijgen om in te loggen via URL.

#### Nog doen:
URLS voor elke gebruiker tonen op reader of dashboard niveau. Of bij gebruiker?
Zorgen dat de gebruiker in het veld *Delen met* alleen gebruikers kan kiezen die hij zelf heeft aangemaakt. Hiervoor module https://www.drupal.org/project/user_created_by gebruiken. Deze is op dit moment nog niet beschikbaar voor Drupal 9.

## Theme Styling
Zie module README.md bestand

## CCR Global
Zie module README.md bestand

## CCR Subscriptions
Zie module README.md bestand

## Aanmaken bij nieuwe opzet
Drie inhoudtypen met velden:
  - **Reader** (`reader`)

    *Domein* (`field_domain`) - Link (Extern)

    *Subdomein* (`field_subdomain`) - Tekst (plat)

    *Thema* (`field_thema`) - Lijst (tekst)

    *Co-Redacteuren* (`field_user`) - Entiteitsreferentie -> Gebruikers

    *Privé* (`field_private`) - Boolean (Conditional Fields: Controlling field)

    *Delen met* (`field_share`) - Entiteitsreferentie -> Gebruikers (Conditional Fields: Target field)
- **Editie** (`edition`)
- **Artikel** (`article`)

Gebruikersrollen:
- **Hoofdredacteur** (`chiefeditor`)
- **Redacteur** (`editor`)
- **Leesgroep** (`reading_group`)

Gebruikersvelden
- **Aangemaakt door** (`field_created_by`) - Entiteitsreferentie -> Gebruikers
- **Abonnement datum** (`field_subscription_date`) - Date -> Date only
- **Abonnement automatisch verlengen** (`field_continuous_subscription`) - Boolean
- **Voornaam** (`field_surname`) - Text
- **Achternaam** (`field_lastname`) - Text (AVG check)
- **Eerste mail** (`field_first_notice`) - Boolean

# FUNCTIONALITEITEN

## Meertaligheid
Meertaligheid kan gewoon geregeld worden via de standaard meertaligheid functionaliteit.
1. Alle 4 modules aanzetten
2. Bij Detectie en selectie Accountbeheerpagina's aanzetten en bovenaanzetten. Ook URL aanzetten en op /en/ zetten.
3. Bij Taal van de inhoud. Content aanvinken en bij Reader de optie `Toon de taalselector op de aanmaken- en bewerk-pagina's` aanzetten. Verder niks

#### Nog doen
Frontpage module controleren bij meertaligheid.

## Mailfunctionaliteit
Toevoegen van artikelen die in een verborgen reader zitten is niet meer mogelijk.
Nieuwsbrieven krijgen een eigen inhoudstype. Waar een titel, beeld en link opgevoerd kunnen worden. En links gemaakt kunnen worden naar artikelen (mits niet verborgen).
